<%@ tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="menu" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<body>


<h1>Portal randkowy</h1>

<div id = "menu">
			<ul>
				<li><a href="demo.html">Twoj profil</a></li>
				<li><a href="demo2.html">Znajomi</a></li>
				<li><a href="demo3.html">Odwiedzajacy</a></li>
				<li><a href="demo4.html">Wyszukiwarka</a></li>
				<li><a href="demo5.html">Wylogoj</a></li>
			</ul>
			<jsp:invoke fragment="menu"/>	
			</div>
			
		</div>
		<div id="content">
			<jsp:doBody/>
		</div>
		
		<div id="footer">
			<script type="text/javascript" src="scripts/jquery-1.11.3.min.js"></script>
			<script type="text/javascript" src="scripts/knockout-3.3.0.js"></script>
			<script type="text/javascript" src="scripts/knockout.mapping.js"></script>
<jsp:invoke fragment="footer"/>	
		</div>
	</body>
</html>
